const fs = require("fs");
const crypto = require("crypto");

const fileStorage = {};

function UploadedFile(name, size, chunksQuantity) {
    this.name = name;
    this.size = size;
    this.chunksQuantity = chunksQuantity;
    this.chunksDone = 0;
    this.chunks = [];
}

UploadedFile.prototype.pushChunk = function(id, file, contentLength, hash) {
    fs.copyFile(file.path,__dirname + '/../tmp/' + hash + '/' + hash +'.chunk_' + id, (err) => {
        if (err) throw err;
    });

    if (file.size !== contentLength) {
        return false;
    }

    this.chunks[id] = __dirname + '/../tmp/' + hash + '/' + hash +'.chunk_' + id;
    this.chunksDone += 1;

    return true;
};

UploadedFile.prototype.isCompleted = function() {
    return this.chunksQuantity === this.chunksDone;
};

function uploading(request, response) {
    if (request.url === '/upload/init') {
        const name = request.fields["display_name"];
        const size = Number(request.fields["size"]);
        const chunksQuantity = Number(request.fields["chunks_count"]);
        const fileId = crypto.randomBytes(32).toString('hex');
        const chunk = request.files.file;

        fileStorage[fileId] = new UploadedFile(name, size, chunksQuantity);
        let file = fileStorage[fileId];

        file.pushChunk(0, chunk, 1024*1024, fileId);

        response.setHeader("Content-Type", "text/text");
        response.write(fileId);
        response.end();

    } else {
        const hash = request.fields["hash"];
        const file = fileStorage[hash];
        const chunkId = request.fields["index"];
        const chunk = request.files.file;
        const chunkSize = Number(chunk.size);

        if (!file) {
            sendBadRequest(response, "Wrong content id header");
            return;
        }
        const chunkComplete = file.pushChunk(chunkId, chunk, chunkSize, hash);

        if (!chunkComplete) {
            sendBadRequest(response, "Chunk uploading was not completed");
            return;
        }

        if (file.isCompleted()) {
            const fstream = fs.createWriteStream(__dirname + '/../files/' + file.name);
            for(let i = 0; i < file.chunksQuantity; i++){
                fstream.write(fs.readFileSync(file.chunks[i]));
                fs.unlink(file.chunks[i], ()=>{});
            }
            fstream.end();
            fs.rmdir(__dirname + '/../tmp/' + hash, () => {});
            delete fileStorage[file];
        }

        response.setHeader("Content-Type", "application/json");
        response.write(JSON.stringify({
            status: 200
        }));
        response.end();
    }
}

function sendBadRequest(response, error) {
    response.write(JSON.stringify({
        status: 400,
        error
    }));
    response.end();
}

exports.uploading = uploading;
