const express = require('express');
const formidable = require('express-formidable');
const url = require("url");
const app = express();
const server = app.listen("3000", () => console.log("Server is running on port 3000 ... "));
const io = require('socket.io')(server);
const socket = io;
const {uploading} = require("./modules/uploadByChank");

app.use(formidable());
app.use(express.static(__dirname + "/public/static"));

app.post("/upload/?*", uploading);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/site.html');
})

app.get('/file/', (req, res) => {
    let q = url.parse(req.url, true).query;
    socket.to(q.id).emit('complete', JSON.stringify(q));
    console.log('stop event sended'+JSON.stringify(q));
    res.send('stop event sended');
})

socket.on('connection', client => {
    newEvent({user: client.id, event: "user_online"});
    client.on('start', data => {
        newEvent({user: client.id, event: "start_import", data: data});
    });
    client.on('disconnect', (data) => {
        newEvent({user: client.id, event: "user_offline"});
    });
});


function newEvent(postData){

    /*
    var clientServerOptions = {
        uri: 'http://localhost:4000/api/files',
        body: JSON.stringify(postData),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        console.log('disconnect event request');
        //console.log(response);
        return true;
    });
    */
    console.log(postData.event);
}

//
/*
let cfg = require('./config.json');
var tw = require('node-tweet-stream')(cfg);
tw.track('socket.io');
tw.track('javascript');
tw.on('tweet', function(tweet){
    io.emit('tweet', tweet);
});
*/
